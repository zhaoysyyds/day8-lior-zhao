package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.exception.EmployeeUpdatedException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    public static final String EMPLOYEE_AGE_RANGE_ERROR_MESSAGE = "Employee must be 18~65 years old";
    public static final String EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE = "Employee over 30 years of age (inclusive) with a salary below 20000 cannot be created";
    public static final String CANNOT_UPDATE_EMPLOYEE = "cannot update employee";
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        if (employee.isAgeRangeInvalid()) {
            throw new EmployeeCreatedException(EMPLOYEE_AGE_RANGE_ERROR_MESSAGE);
        }
        if (employee.isAgeAndSalaryInvalid()) {
            throw new EmployeeCreatedException(EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE);
        }
        //employee.setActive(true);
        return employeeRepository.save(employee);
    }

    public Employee deleteEmployee(Long id) {
        Employee employee = employeeRepository.findById(id);
        employee.setStatus(false);
        return employee;
    }

    public Employee update(Long id, Employee employeeMessage) {
        Employee employee = employeeRepository.findById(id);
        if (employee.getStatus()) {
            return employeeRepository.update(id, employeeMessage);
        } else {
            throw new EmployeeUpdatedException(CANNOT_UPDATE_EMPLOYEE);
        }
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getEmployeeByPageAndSize(int page, int size) {
        return employeeRepository.findEmployeeByPageAndSize(page, size);
    }

    public List<Employee> getEmployeeByCompanyId(long id) {
        return employeeRepository.getByCompanyId(id);
    }

    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getEmployeesByCompanyId(long id) {
        return employeeRepository.getByCompanyId(id);
    }

    public void deleteEmployeeByCompanyId(long id) {
        employeeRepository.updateEmployeeStatusByCompanyId(id);
    }
}
