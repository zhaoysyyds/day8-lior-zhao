package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Resource
    private EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeService.create(employee);
    }


    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getAllEmployee();
    }


    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeService.getEmployeeById(id);
    }


    @GetMapping(params = {"gender"})
    public List<Employee> findEmployees(@RequestParam String gender) {
        return employeeService.getEmployeesByGender(gender);
    }


    @PutMapping(value = "/{id}")
    public Employee updateEmployee(@PathVariable Long id, @RequestBody Employee employeeMessage) {
        return employeeService.update(id, employeeMessage);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
    }


    @GetMapping(params = {"page", "size"})
    public List<Employee> findEmployeesByPageAndSize(@RequestParam int page, @RequestParam int size) {
        if (page <= 0 || size <= 0) return null;
        return employeeService.getEmployeeByPageAndSize(page, size);
    }
}
