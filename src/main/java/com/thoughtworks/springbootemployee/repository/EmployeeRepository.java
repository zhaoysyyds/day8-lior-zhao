package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId)).map(employee -> employee.getId() + 1).orElse(1L);
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst().orElse(null);

    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public Employee update(Long id, Employee employeeMessage) {
        Employee findEmployee = employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst().orElse(null);
        if (Objects.nonNull(employeeMessage.getAge()))
            findEmployee.setAge(employeeMessage.getAge());
        if (Objects.nonNull(employeeMessage.getSalary()))
            findEmployee.setSalary(employeeMessage.getSalary());
        if (Objects.nonNull(employeeMessage.getCompanyId()))
            findEmployee.setCompanyId(employeeMessage.getCompanyId());
        return findEmployee;
    }

    public void deleteEmployee(Long id) {
        employees.removeIf(employee -> Objects.equals(employee.getId(), id));
    }


    public List<Employee> findEmployeeByPageAndSize(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getByCompanyId(long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), id))
                .collect(Collectors.toList());
    }


    public void updateEmployeeStatusByCompanyId(long id) {
        employees.stream().forEach(employee -> {
            if (Objects.equals(employee.getCompanyId(), id)) {
                employee.setStatus(false);
                employee.setCompanyId(null);
            }
        });
    }

    public void deleteAll() {
        employees.clear();
    }
}
