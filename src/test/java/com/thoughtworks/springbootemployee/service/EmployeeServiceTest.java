package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.exception.EmployeeUpdatedException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class EmployeeServiceTest {
    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_craeted_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) {

        Employee employee = new Employee(null, "Lily", age, "female", 20000);


        when(employeeRepository.save(employee))
                .thenReturn(new Employee(1L, "Liy", age, "female", 20000));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Liy", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
    }

    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "female", 20000);
        //when
        //then
        EmployeeCreatedException employeeCreatedException =
                assertThrows(EmployeeCreatedException.class,
                        () -> employeeService.create(employee));
        assertEquals("Employee must be 18~65 years old", employeeCreatedException.getMessage());
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30,20000", "29,19999"})
    void should_return_created_employee_when_create_employee_given_employee_age_is_30_and_salary_is_20000(Integer age, Integer salary) {
        Employee employee = new Employee(null, "Lily", age, "female", salary);
        when(employeeRepository.save(employee))
                .thenReturn(new Employee(1L, "Lily", age, "female", salary));
        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);

        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());
    }

    @Test
    void should_throw_exception_when_create_employee_given_employee_age_is_30_and_salary_is_10000() {
        Employee employee = new Employee(null, "Lily", 30, "female", 10000);

        EmployeeCreatedException employeeCreatedException =
                assertThrows(EmployeeCreatedException.class,
                        () -> employeeService.create(employee));
        assertEquals("Employee over 30 years of age (inclusive) with a salary below 20000 cannot be created", employeeCreatedException.getMessage());
    }


    @Test
    void should_return_created_employee_when_create_employee_given_employee_age_is_20_and_salary_is_10000() {
        Employee employee = new Employee(null, "Lily", 20, "female", 10000, false);

        when(employeeRepository.save(employee))
                .thenReturn(new Employee(1L, "Lily", 20, "female", 10000, true));

        Employee employeeResponse = employeeService.create(employee);


        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(20, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(10000, employeeResponse.getSalary());
        assertTrue(employeeResponse.getStatus());
    }


    @Test
    void should_return_nothing_when_delete_employee_given_employee_id() {
        Employee employee = new Employee(null, "Lily", 20, "female", 10000);

        when(employeeRepository.findById(any()))
                .thenReturn(new Employee(1L, "Lily", 20, "female", 10000, false));
        Employee employeeResponse = employeeService.deleteEmployee(1L);
        assertFalse(employeeResponse.getStatus());
        assertNotNull(employeeResponse);
        assertEquals(false, employeeResponse.getStatus());
        assertEquals(employeeResponse.getId(), 1);
        assertEquals(employeeResponse.getAge(), employee.getAge());
        assertEquals(employeeResponse.getGender(), employee.getGender());
        assertEquals(employeeResponse.getName(), employee.getName());
    }

    @Test
    void should_return_nothing_when_update_employee_given_employee_is_not_active() {
        Employee employee = new Employee(1L, "Lily", 20000, "female", 10000);

        when(employeeRepository.findById(any()))
                .thenReturn(new Employee(1L, "Lilly", 20, "female", 10000, false));
        when(employeeRepository.update(any(), any()))
                .thenReturn(new Employee(1L, "Lilly", 20, "female", 1000));


        EmployeeUpdatedException employeeUpdatedException =
                assertThrows(EmployeeUpdatedException.class,
                        () -> employeeService.update(1L, employee));
        assertEquals("cannot update employee", employeeUpdatedException.getMessage());

    }


}
