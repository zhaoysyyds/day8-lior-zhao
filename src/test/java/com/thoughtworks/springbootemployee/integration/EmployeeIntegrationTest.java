package com.thoughtworks.springbootemployee.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeIntegrationTest {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        employeeRepository.deleteAll();
    }
    @Test
    void should_get_all_employees_when_perform_get_all_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        Employee employee2 = new Employee(1L, "Lucy", 200, "female", 30000, 0L);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(3000));
    }

    @Test
    void should_create_employee_when_perform_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
        Employee savedEmployee = employeeRepository.findAll().get(0);
        assertEquals("Lucy", savedEmployee.getName());
        assertEquals(20, savedEmployee.getAge());
        assertEquals("female", savedEmployee.getGender());
        assertEquals(3000, savedEmployee.getSalary());
    }

    @Test
    void should_update_employee_when_perform_update_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        Employee updateEmployee = new Employee(1L, "Lucy", 2000, "female", 3000, 0L);
        String updateEmployeeJson = new ObjectMapper().writeValueAsString(updateEmployee);
        employeeRepository.save(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.put("/employees/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(2000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
        Employee findEmployee = employeeRepository.findAll().get(0);
        assertEquals("Lucy", findEmployee.getName());
        assertEquals(2000, findEmployee.getAge());
        assertEquals("female", findEmployee.getGender());
        assertEquals(3000, findEmployee.getSalary());
    }

    @Test
    void should_delete_employee_when_perform_delete_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        employeeRepository.save(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().is(204));
        List<Employee> employees = employeeRepository.findAll();
        assertEquals(1, employees.size());
        assertEquals(false, employees.get(0).getStatus());
    }

    @Test
    void should_get_employee_by_id_when_perform_get_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        employeeRepository.save(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
    }

    @Test
    void should_get_employees_by_gender_when_perform_get_employee_by_gender_given_employee() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "male", 3000, 0L);
        Employee employee2 = new Employee(1L, "Lucy", 20, "female", 2000, 0L);
        Employee employee3 = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?gender={gender}", "female"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id", containsInAnyOrder(2, 3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].salary", containsInAnyOrder(2000, 3000)));
    }

    @Test
    void should_get_employees_by_page_and_size_when_perform_get_employee_by_page_and_size_given_employee() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "male", 3000, 0L);
        Employee employee2 = new Employee(1L, "Lucy", 20, "female", 2000, 0L);
        Employee employee3 = new Employee(1L, "Lucy", 20, "female", 3000, 0L);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?page=1&size=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id", containsInAnyOrder(1, 2)));
    }
}
