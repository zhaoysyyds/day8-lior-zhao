package com.thoughtworks.springbootemployee.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyIntegrationTest {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc mockMvc;


    @BeforeEach
    public void setUp() {
        companyRepository.deleteAll();
    }


    @Test
    void should_get_all_companies_when_perform_get_all_companies() throws Exception {
        //given
        companyRepository.save(new Company("company1"));
        companyRepository.save(new Company("company2"));
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("company1"));
    }

    @Test
    void should_create_companies_when_perform_create_company_given_company() throws Exception {
        //given
        Company company = new Company("company1");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("company1"));
        Company saveCompany = companyRepository.getCompanies().get(0);
        assertEquals("company1", saveCompany.getName());
    }


    @Test
    void should_get_company_by_id_when_perform_get_company_by_id() throws Exception {
        //given
        Company company = new Company("company1");
        companyRepository.save(company);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/" + company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("company1"));
    }

    @Test
    void should_get_employees_by_company_id_when_perform_get_employees_by_company_id() throws Exception {
        //given
        Company company = new Company("company1");
        companyRepository.save(company);
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000, 1L);
        Employee employee2 = new Employee(1L, "Luc", 20, "female", 3000, 2L);
        Employee employee3 = new Employee(1L, "Lu", 20, "female", 3000, 1L);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/" + company.getId() + "/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lucy"));
    }

    @Test
    void should_get_companies_by_page_and_size_when_perform_get_companies_by_page_and_size_given_companies() throws Exception{
        //given
        Company company1 = new Company("company1");
        Company company2 = new Company("company2");
        Company company3 = new Company("company3");
        companyRepository.save(company1);
        companyRepository.save(company2);
        companyRepository.save(company3);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?page=2&size="))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("company3"));

    }
    @Test
    void should_update_company_when_perform_update_company_given_company() throws Exception {
        //given
        Company company = new Company("company1");
        Company companyMessage = new Company("company222");
        companyRepository.save(company);
        String companyJson = new ObjectMapper().writeValueAsString(companyMessage);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/" + company.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("company222"));
        Company saveCompany = companyRepository.getCompanies().get(0);
        assertEquals("company222", saveCompany.getName());
    }

    @Test
    void should_delete_company_when_perform_delete_company_given_company_and_employees() throws Exception {
        //given
        Company company = new Company("company1");
        companyRepository.save(company);
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000, 1L);
        Employee employee2 = new Employee(1L, "Luc", 20, "female", 3000, 1L);
        Employee employee3 = new Employee(1L, "Lu", 20, "female", 3000, 1L);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/" + company.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        List<Employee> employees = employeeRepository.findAll().stream().filter(employee -> employee.getStatus()).collect(Collectors.toList());
        assertEquals(0, employees.size());
    }
}
